import Banner from '../components/Banner';
import Highlights from '../components/Highlights'

/*-----------------------S53 Activity----------------------------*/
export default function Home() {
	return (
		<>
		< Banner
		title="Zuitt Coding Bootcamp"
		subtitle= "Opportunities for everyone, everywhere."
		destination = "/courses"
		button= "Enroll now!"
		/>
		< Highlights/>
		</>
	)
}
/*-----------------------S53 Activity END------------------------*/