// import Button from 'react-bootstrap/Button';
// import Row from 'react-bootstrap/Row';
// import Col from 'react-bootstrap/Col';

import { Button, Row, Col } from 'react-bootstrap';
import {Link} from 'react-router-dom';

/*-----------------------S53 Activity----------------------------*/
export default function Banner(showBanner) {

return (
    <Row>
    	<Col className="p-5">
            <h1>{showBanner.title}</h1>
            <p>{showBanner.subtitle}</p>
            <Button variant="primary" as={Link} to={showBanner.destination}>{showBanner.button}</Button>
        </Col>
    </Row>
	)
}
/*-----------------------S53 Activity END------------------------*/