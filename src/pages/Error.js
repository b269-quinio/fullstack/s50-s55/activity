/*-----------------------S53 Activity--------------------------*/

/*import {Navigate, Link} from 'react-router-dom';
import {Row, Col, Button} from 'react-bootstrap';


export default function Error(){

	return (
	    <Row>
	    	<Col className="p-5">
	            <h1>Error 404 - Page not found.</h1>
	            <p>The page you are looking for cannot be found.</p>
	            <Button variant="primary" as={Link} to="/">Back to Home</Button>
	        </Col>
	    </Row>
		)
	}
*/
/*-----------------------S53 Activity END------------------------*/

/*-----------------------S53 Activity----------------------------*/
// Achieving Reusable Banner instruction

import Banner from '../components/Banner';

export default function Error() {
	return (
		< Banner
		title="Error 404 - Page not found."
		subtitle= "The page you are looking for cannot be found."
		destination = "/"
		button= "Back to Home"
		/>
	)
};

/*-----------------------S53 Activity END------------------------*/