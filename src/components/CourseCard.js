// [S50 ACTIVITY]

/*import {Button, Card} from 'react-bootstrap';

export default function CourseCard() {
  return (
    <Card className="mt-3 mb-3">
      <Card.Img/>
      <Card.Body>
        <Card.Title>
          <h4>Sample Course</h4>
        </Card.Title>
        <Card.Text>
          <h5>Description:</h5>
          <p>This is a sample course offering.</p>
          <h5>Price:</h5>
          <p>PhP 40,000</p>
        </Card.Text>
        <Button variant="primary">Enroll</Button>
      </Card.Body>
    </Card>
  );
}*/

// [S50 ACTIVITY END]

import {useState, useEffect} from 'react';
import { Button, Row, Col, Card } from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function CourseCard({course}) {

  //  Deconstruct the course properties into their own variables
  const { name, description, price, _id } = course;

  /*
  SYNTAX:
    const [getter, setter] = useState(initialGetterValue);
  */

/*----------S51 ACTIVITY------------*/
//   const [count, setCount] = useState(30);

//   function enroll () {
//     if (count == 0){
//       return alert("No more seats!");
//     } else {
//     setCount(count - 1);
//   }
// };
/*--------S51 ACTIVITY END----------*/

//   const [count, setCount] = useState(0);
//   const [seats, setSeats] = useState(5);

//   function enroll () {
//     setCount(count + 1);
//     setSeats(seats - 1);
//   };

// useEffect(() => {
//   if(seats <= 0){
//     alert("No more seats available!")
//   }
// }, [seats]);


return (
    <Row className="mt-3 mb-3">
            <Col xs={12}>
                <Card className="cardHighlight p-0">
                    <Card.Body>
                        <Card.Title><h4>{name}</h4></Card.Title>
                        <Card.Subtitle>Description</Card.Subtitle>
                        <Card.Text>{description}</Card.Text>
                        <Card.Subtitle>Price</Card.Subtitle>
                        <Card.Text>{price}</Card.Text>
{/*                     <Card.Subtitle>Enrollees </Card.Subtitle>
                        <Card.Text>{count} Enrollees</Card.Text>*/}
                        {/*<Card.Subtitle>Count: {count} </Card.Subtitle>
                        <Card.Subtitle>Seats: {seats} </Card.Subtitle>*/}
                        {/*<Button variant="primary" onClick={enroll} disabled={seats<=0}>Enroll</Button>*/}
                        <Button className="bg-primary" as={Link} to={`/courses/${_id}`} >Details</Button>
                    </Card.Body>
                </Card>
            </Col>
    </Row>        
    )
}
